---
layout: handbook-page-toc
title: FY22-Q4 L&D Newsletter
---

Welcome to the last GitLab Learning & Development (L&D) Newsletter! The purpose of the L&D newsletter is to enable a culture of curiosity and continuous learning that *prioritizes learning* with a [growth mindset](/handbook/values/#growth-mindset) for team members. We are currently evaluating how to give more frequent updates on what our team is working on. 

You can find links to [past L&D Newsletters](/handbook/people-group/learning-and-development/newsletter/#past-newsletters). 

## Recap of Q3

* [Crucial Conversations Cohort 2](/handbook/people-group/learning-and-development/learning-initiatives/#crucial-conversations) 
* [FY22-Q3 Mental Health Newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q3/) 
* People Group Learning Hub
* Development Learning Hub
* [Improved mentorship documentation for team members](https://about.gitlab.com/handbook/people-group/learning-and-development/mentor/)
* [2nd iteraton of Women at GitLab mentorship program](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/)
* Completed the [Fifth Iteration of the Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/), and over 20 [Managers and Aspiring Managers](https://docs.google.com/presentation/d/1s_yNEKLVDJ-nYTsvD568lpz1Bfan6qIZoZrY0ZEEkHI/edit#slide=id.gd2286d9050_0_7) completed the training!

## Upcoming in Q4

* Crucial Conversations Cohort with the Security Team
* [LifeLabs Learning](https://lifelabslearning.com/) Pilot (More Details to Come!) 
* 3 Live Speaker Series - watch the team member calendar for invites!
     1. November: Discussing building internal culture with team members at Google
     1. November: Managing on a remote team
     1. December: Mental health awareness week speakers December 13th to 16th

## Learning Spotlight 

[Crucial Conversations](/handbook/leadership/crucial-conversations/) is a great way to kickstart your learning! 

If you are interested in participating in Crucial Conversations in the future, please fill out [this form](https://docs.google.com/forms/d/e/1FAIpQLSdqwibbQZs-zL-IX9aq9Yzgozm-y3i0Vwh59T8T1nR74mxmFQ/viewform). If you would like your team to go through the trianing together, please reach out to our team in the `#learninganddevelopment` Slack Channel. 

## Learning Tips 

If you aren't sure where to start with learning, try out **LinkedIn Learning**. You can go to the website and search by skill or interest and it will bring up all related courses. 

Some recommendations of where to find LinkedIn Learning courses: 

* [Skill of the Month](https://about.gitlab.com/handbook/people-group/learning-and-development/learning-initiatives/#skill-of-the-month) has a variety of different LinkedIn Learning courses broken up by topic 
* Take a look at what we did for our [Career Development Skill of the Month](/handbook/people-group/learning-and-development/career-development/#skill-of-the-month) 
* Use the [LinkedIn Learning website](https://www.linkedin.com/learning/) to search for any courses that you may be interested in.

## Discussion 

If you would like to discuss items related to this newsletter, please see the related [issue](https://gitlab.com/gitlab-com/people-group/learning-development/newsletter/-/issues/13). 
