require 'spec_helper'
require 'fileutils'
require_relative '../../lib/categories/categories_yml_link_checker'

describe Categories::CategoriesYmlLinkChecker do
  before do
    allow(subject).to receive(:log)
  end

  describe 'success' do
    it 'succeeds' do
      # Use the real file in the success case, it will integration test against
      # unexpected formatting changes.
      expect(subject).to receive(:log).with(/SUCCESS: categories.yml links are valid/)
      subject.run
    end
  end

  describe 'failure' do
    it 'fails' do
      tmpdir = Dir.tmpdir
      bad_categories_contents = <<~BAD_YAML
        bad_category:
          name: "Missing Docs Category"
          maturity: minimal
          marketing: true
      BAD_YAML
      system("echo '#{bad_categories_contents}' > #{tmpdir}/bad_categories.yml") ||
        raise('Error creating bad_categories.yml fixture file')
      expected_exception_msg = 'ERROR: categories.yml had 1 bad categories with invalid links'
      expect { subject.run("#{tmpdir}/bad_categories.yml") }.to raise_error(expected_exception_msg)
      expected_line_msg = /1. Bad Category: 'Missing Docs Category' at 'minimal' maturity with no documentation link./
      expect(subject).to have_received(:log).with(expected_line_msg)
    end
  end
end
